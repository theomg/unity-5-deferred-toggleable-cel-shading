# Per material cel shading for Unity 5 Deferred path #

![celshading.jpg](https://bitbucket.org/repo/oadaAj/images/301596197-celshading.jpg)

*Cel shaded body, face shaded normally*




## Details ##
To use select the CustomStandard material and use the "Is Celshaded?" checkbox. Also, on the Project Settings > Graphics window, select "Custom shader" under Deferred and plug the "Internal-DeferredShading" from  the "Resources" folder on the slot.

The shaders were modified from the builtin unity 5.3.4f1, adaptations may be needed for future versions.

All modified code has a "//CEL" comment.

These shaders use the otherwise unused alpha of the Normal GBuffer to store the cel shading mask.